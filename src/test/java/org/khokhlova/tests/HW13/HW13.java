package org.khokhlova.tests.HW13;

import org.khokhlova.tests.Base.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HW13 extends Base {
    @BeforeMethod
    public void beforeMethod(){
        driver.get("https://the-internet.herokuapp.com/nested_frames");
    }
    @AfterMethod
    public void afterMethod(){
        driver.switchTo().defaultContent();
    }
    @Test (dataProvider = "frames")
    public void goToFrames(String nameOfFrame, String expectedBodyResult ){
       if (!nameOfFrame.equals("frame-bottom")){
            driver.switchTo().frame("frame-top");
     }
        driver.switchTo().frame(nameOfFrame);

        WebElement body = driver.findElement(By.tagName("body"));
        String bodyText = body.getText().trim();

        Assert.assertEquals(bodyText, expectedBodyResult);
    }
@DataProvider(name = "frames")
    public Object[][] frames(){
     return new Object[][]{
             {"frame-left", "LEFT"},
             {"frame-middle", "MIDDLE"},
             {"frame-right", "RIGHT"},
             {"frame-bottom", "BOTTOM"}
     };
}
}

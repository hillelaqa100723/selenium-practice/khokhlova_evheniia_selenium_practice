package org.khokhlova.tests.hw11;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HW11 extends Base {

    WebDriver driver;


    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
    }


    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }


    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }


    @Test(description= "Click on Ok button for JS_Alert with JS")
    public void jsAlertTestWithJS() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";

        clickJSButtonWithJs(Buttons.JS_Alert);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }
    @Test(description= "Click on Cancel button for JS_Alert with JS")
    public void jsAlertTestWithJS2() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";

        clickJSButtonWithJs(Buttons.JS_Alert);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }
    @Test(description= "Click on Ok button for for JS Confirm with JS")
    public void jsConfirmTestWithJS() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";

        clickJSButtonWithJs(Buttons.JS_Confirm);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }
    @Test(description= "Click on Ok button for for JS Confirm with JS")
    public void jsConfirmTestWithJS2() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";

        clickJSButtonWithJs(Buttons.JS_Confirm);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }


        @Test(description = "Click on OK button for JS Confirm")
    public void jsAlertConfirm() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";


        clickJSButton(Buttons.JS_Confirm);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);

    }


    @Test(description = "Click on Cancel button for JS Confirm")
    public void jsAlertCancel() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";


        clickJSButton(Buttons.JS_Confirm);
        String text = workWithAlert(false);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultText(), expectedResultText);


    }
    @Test(description = "Click on Ok button for JS Prompt with text to enter")
    public void jsPromptEnterTextJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButtonWithJs(Buttons.JS_Prompt);
        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test(description = "Click on Ok button for JS Prompt without text to enter")
    public void jsPromptWithoutEnterTextJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButtonWithJs(Buttons.JS_Prompt);
        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }
    @Test(description = "Click on Cancel button for JS Prompt with text to enter")
    public void jsPromptWithEnterTextCancelJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Test!";

        clickJSButtonWithJs(Buttons.JS_Prompt);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + null, resultText);
    }
    @Test(description = "Click on Cancel button for JS Prompt without text to enter")
    public void jsPromptWithoutEnterTextCancelJs() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButtonWithJs(Buttons.JS_Prompt);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + null, resultText);
    }
    @Test(description = "Add text to JS prompt")
    public void jsPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "text";
        clickJSButton(Buttons.JS_Prompt);


        String textFromAlert = workWithAlert(true, testToEnter);


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }


    @Test(description = "Check empty for JS Prompt")
    public void jsPromptEmpty() {
        String expectedTextAtAlert = "I am a JS prompt";


        clickJSButton(Buttons.JS_Prompt);


        String textFromAlert = workWithAlert(true, "");


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:", resultText);
    }


    @Test(description = "Add text to JS Prompt and click Cancel")
    public void JsPromptPlusCancel() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "text";


        clickJSButton(Buttons.JS_Prompt);


        String textFromAlert = workWithAlert(false, testToEnter);


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);


    }


    @Test(description = "Check empty field plus Cancel for JS Prompt")
    public void JsPromptEmptyPlusCancel() {
        String expectedTextAtAlert = "I am a JS prompt";


        clickJSButton(Buttons.JS_Prompt);


        String textFromAlert = workWithAlert(false, "");


        String resultText = getResultText();


        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: null", resultText);


    }
    public enum Buttons {
        JS_Alert("Click for JS Alert", "jsAlert()"),
        JS_Confirm("Click for JS Confirm", "jsConfirm()"),
        JS_Prompt("Click for JS Prompt", "jsPrompt()");

        private String textValue;
        private String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }



    private WebElement findButton(Buttons button) {
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }
    private void clickJSButton(Buttons buttons) {

        findButton(buttons).click();
    }
    private void clickJSButtonCallingJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("return " + buttons.getCode());
    }

    private void clickJSButtonWithJs(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement button = findButton(buttons);
        executor.executeScript("return arguments[0].click();", button);
    }




    private String workWithAlert(boolean accept, String... textToInput) {
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();


        if (textToInput.length > 0) {
            alert.sendKeys(textToInput[0]);
        }


        if (accept) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }


    private String getResultText() {
        return driver.findElement(By.id("result")).getText();
    }
    private String getResultTextWithJs() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("result"));
        return executor.executeScript("return arguments[0].textContent;", element).toString();
    }

}


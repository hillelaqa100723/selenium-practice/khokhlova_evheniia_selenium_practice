package org.khokhlova.tests.Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Base {
    protected WebDriver driver;
    @BeforeSuite
    public void beforeSuite() {
        driver = new ChromeDriver();
    }

    @AfterSuite
    public void afterSuite() {
        if (driver!= null) {
            driver.quit();
        }
    }
}
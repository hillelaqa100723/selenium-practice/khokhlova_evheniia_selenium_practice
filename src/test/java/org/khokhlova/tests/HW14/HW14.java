package org.khokhlova.tests.HW14;

import org.khokhlova.tests.Base.Base;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Set;

public class HW14 extends Base {
    @BeforeMethod
    public void beforeMethod(){
        driver.get("http://the-internet.herokuapp.com/windows");
    }
    @Test
    public void windowTest() {
        String mainWindowHandle= driver.getWindowHandle(); //remember main handle
        driver.findElement(By.linkText("Click Here")).click(); //click on the active element
        Set<String> windowHandles = driver.getWindowHandles(); //get handles
        for (String handle: windowHandles){
            if(!handle.equals(mainWindowHandle)){
                driver.switchTo().window(handle);//decide what handle use to switch (not main)
                break;
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(100));

        String text = driver.findElement(By.xpath("//h3[contains(text(),'New Window')]")).getText(); //get info for result
        String currentUrl = driver.getCurrentUrl();//get info for result

        Assert.assertEquals(text, "New Window");
        Assert.assertTrue(currentUrl.endsWith("/new"));

        driver.close(); //close current window

        driver.switchTo().window(mainWindowHandle); //switch to main window


        text = driver.findElement(By.xpath("//h3[contains(text(),'Opening a new window')]")).getText(); //get info for result

        Assert.assertEquals(text,"Opening a new window");
        Assert.assertTrue(driver.findElement(By.linkText("Click Here")).isDisplayed());
    }
}

